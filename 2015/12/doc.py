with open("input.txt") as f:
    full = f.readlines()

for char in ":\{\}[],\"":
    full = [i for el in full for i in el.split(char)]
digits = [int(el) for el in full if el.lstrip('-').isdigit() == True]
print(sum(digits))

###################################

with open("input.txt") as f:
# with open("example.txt") as f:
    full = f.readlines()[0].replace(" ", "")

red = full.find(':"red"')
while red > 0:# and open < len(full):

    # print(full[red - 5 : red + 10], red)

    # Find opening of dict
    brackets, open = 0, red
    while brackets >= 0:
        open -= 1
        if full[open] == '{':
            brackets -= 1
        elif full[open] == '}':
            brackets += 1
    open = open - 1 if full[open - 1] == ':' else open
    
    # Find closing of dict
    brackets, close = 0, red
    while brackets >= 0:
        if full[close] == '{':
            brackets += 1
        elif full[close] == '}':
            brackets -= 1
        close += 1

    full = full[:open] + full[close:]
    red = full.find(':"red"')

full = [full]
for char in ":\{\}[],\"":
    full = [i for el in full for i in el.split(char)]
digits = [int(el) for el in full if el.lstrip('-').isdigit() == True]
print(sum(digits))