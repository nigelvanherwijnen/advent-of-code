with open("input.txt") as f:
    floors = f.readlines()

print(floors)

# Result part 1
print(floors[0].count('(') - floors[0].count(')'))

floor, pos = 0, 0
while floor >= 0:
    pos += 1
    floor += 1 if floors[0][pos - 1] == '(' else -1

# Result part 2
print(pos)