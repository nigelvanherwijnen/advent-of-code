with open("input.txt") as f:
# with open("example.txt") as f:
    signals = f.read().splitlines()
signals = [command.split(" -> ") for command in signals]
signals = [[command[0].split(" "), command[1]] for command in signals]
wires = dict()
# print(signals)

LSHIFT = lambda x, y: x << y
RSHIFT = lambda x, y: x >> y
AND    = lambda x, y: x & y
OR     = lambda x, y: x | y
NOTT    = lambda x: ~x

def get_value(el):
    if el.isdigit():
        return int(el) 
    return wires[el]

while "a" not in wires.keys():
    # print(wires)
    print(len(signals))
    i = -1
    done = []

    for signal in signals:
        command, target = signal
        # command = [int(el) if el.isdigit() else el for el in command]
        # print(command)
        i += 1

        if len(command) == 1 and command[0].isdigit():
            wires[target] = get_value(command[0])
        elif len(command) == 1 and command[0] in wires.keys():
            wires[target] = get_value(command[0])
        elif len(command) == 1:
            continue 

        elif len(command) == 2 and command[1] in wires.keys():
            wires[target] = NOTT(get_value(command[1]))
        elif len(command) == 2:
            continue 

        elif (command[1] == "LSHIFT" or command[1] == "RSHIFT") and not (command[0] in wires.keys()):
            continue
        elif command[1] == "LSHIFT":
            wires[target] = LSHIFT(get_value(command[0]), get_value(command[2]))
        elif command[1] == "RSHIFT":
            wires[target] = RSHIFT(get_value(command[0]), get_value(command[2]))

        elif (command[1] == "AND" or command[1] == "OR") and \
            ((not command[0].isdigit() and not command[0] in wires.keys()) or \
            (not command[2].isdigit() and not command[1] in wires.keys())):
            continue 
        elif command[1] == "AND":
            wires[target] = AND(get_value(command[0]), get_value(command[2]))
        elif command[1] == "OR":
            wires[target] = OR(get_value(command[0]), get_value(command[2]))

        done.append(i)

    done = reversed(sorted(done))
    for el in done:
        signals.pop(el)

    # break

print(wires)


