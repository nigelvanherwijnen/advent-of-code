# input = "hijklmmm"
# input = "abbceffg"
# input = "abbcegjk"
# input = "abcdefgh"
# input = "ghijklmn"
# input = "hxbxwxba"
input = "hxbxxyzz"

def validity(password):

    i, doubles = 0, 0
    while i + 1 < len(password) and doubles != 2:
        if password[i] == password[i + 1]:
            doubles += 1
            i += 1
        i += 1
    if doubles != 2:
        return False

    i = 0
    while i + 2 < len(password):
        if ord(password[i + 1]) - ord(password[i]) == 1 and \
            ord(password[i + 2]) - ord(password[i + 1]) == 1:
            return True
        i += 1
    return False
    
def end_loop(password):
    for el in password:
        if el != 'z':
            return True 
    return False

def increment(character):
    alph = 26
    return chr((ord(character) + 1 - ord('a')) % alph + ord('a'))

key = [char for char in input]
key_len = len(key)
while end_loop(key):

    i = len(key) - 1

    key[i] = increment(key[i])
    while key[i] == 'a':
        i -= 1
        key[i] = increment(key[i])
    if key[i] in ['i', 'o', 'l']:
        key[i] = increment(key[i])

    if validity(key):
        print("".join(key))
        break
