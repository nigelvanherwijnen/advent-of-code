with open("input.txt") as f:
# with open("example.txt") as f:
    routes = f.read().splitlines()

# Get distances
distances = dict()
for route in routes:
    route = route.split()
    if route[0] not in distances.keys():
        distances[route[0]] = dict()
    if route[2] not in distances.keys():
        distances[route[2]] = dict()

    distances[route[0]][route[2]] = int(route[4])
    distances[route[2]][route[0]] = int(route[4])

# Generate all permutations (omg yields wtf)
def permutation(elements):
    if len(elements) == 1:
        yield elements 
    else:
        for conf in permutation(elements[1:]):
            for i in range(len(elements)):
                yield conf[:i] + elements[0:1] + conf[i:]

# Calc all distances
cities = distances.keys()
options = permutation(list(cities))
min_route = 10000
max_route = 0
for option in options:
    dist = 0
    for i in range(len(option) - 1):
        dist += distances[option[i]][option[i + 1]]
    min_route = dist if dist < min_route else min_route 
    max_route = dist if dist > max_route else max_route

print(min_route, max_route)
