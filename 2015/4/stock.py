import hmac, hashlib

key = "iwrupvqb"
i = 0
while result := hashlib.md5((key + str(i := i + 1)).encode("utf-8")).hexdigest():
    count = 0
    for j in range(6):
        if result[j] == '0':
            count += 1
    if count == 6:
        print(i)
        break
