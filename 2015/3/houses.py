from operator import  add
with open("input.txt") as f:
    moves = f.readlines()[0]
# print(moves)
# moves = moves[::100]

directions = {
    '>': (0, 1),
    '<': (0, -1),
    '^': (1, 0),
    'v': (-1, 0)
}

loc = [(0, 0)]
for direction in moves:
    loc.append(tuple(map(add, loc[-1], directions[direction])))

# Result part 1
print(len(set(loc)))

loc_santa, loc_robo = [(0, 0)], [(0, 0)]
for i in range(0, len((moves)), 2):
    loc_santa.append(tuple(map(add, loc_santa[-1], directions[moves[i]])))
    loc_robo.append(tuple(map(add, loc_robo[-1], directions[moves[i + 1]])))
loc_all = loc_santa + loc_robo

# Result part 2
print(len(set(loc_all)))
