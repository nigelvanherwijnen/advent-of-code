# input = "1"
# input = "11"
# input = "21"
# input = "1211"
# input = "111221"
input = "1113122113"

##### SLOW ######
# for i in range(50):
#     output = []
#     while len(input) > 0:
#         # print(input, output)
#         output.append(str(len(input) - len(input.lstrip(input[0]))))
#         output.append(input[0])
#         input = input.lstrip(input[0])
#     input = "".join(output)

#     print(len(input))

size = 360154
for i in range(10):
    size *= 1.30357
print(size)

# > 5.103.073, > 5.103.358