import numpy as np

with open("input.txt") as f:
    orders = f.read().splitlines()

# orders = [
#     "turn on 0,0 through 9,9",
#     "toggle 0,0 through 9,0",
#     "turn off 4,4 through 5,5"
# ]

grid = np.zeros((1000, 1000))

for order in orders:
    order = order.split(" ")
    order = order[1::] if order[0] == "turn" else order
    # print(order)

    start = [int(el) for el in order[1].split(",")]
    end = [int(el) + 1 for el in order[3].split(",")]

    if order[0] == "on":
        grid[start[0]:end[0], start[1]:end[1]] = 1
    elif order[0] == "off":
        grid[start[0]:end[0], start[1]:end[1]] = 0
    else:
        for i in range(start[0], end[0]):
            for j in range(start[1], end[1]):
                grid[i, j] = 0 if grid[i, j] == 1 else 1
    # print(grid)

# Result part 1
print(sum(sum(grid)))


grid = np.zeros((1000, 1000))

for order in orders:
    order = order.split(" ")
    order = order[1::] if order[0] == "turn" else order
    # print(order)

    start = [int(el) for el in order[1].split(",")]
    end = [int(el) + 1 for el in order[3].split(",")]

    if order[0] == "on":
        grid[start[0]:end[0], start[1]:end[1]] += 1
    elif order[0] == "off":
        grid[start[0]:end[0], start[1]:end[1]] -= 1
        grid[grid < 0] = 0
    else:
        grid[start[0]:end[0], start[1]:end[1]] += 2
    # print(grid)

# Result part 2
print(sum(sum(grid)))