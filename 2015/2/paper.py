with open("input.txt") as f:
    gifts = f.readlines()

gifts = [sorted([int(side) for side in gift.strip().split('x')]) for gift in gifts]
paper = [sorted([gift[0] * gift[1], gift[0] * gift[2], gift[1] * gift[2]]) for gift in gifts]
paper = [3 * gift[0] + 2 * gift[1] + 2 * gift[2] for gift in paper]

# Result part 1
print(sum(paper))

# Result part 2
ribbon = [2 * gift[0] + 2 * gift[1] + gift[0] * gift[1] * gift[2] for gift in gifts]
print(sum(ribbon))