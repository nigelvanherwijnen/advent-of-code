with open("input.txt") as f:
    nice = f.read().splitlines()
# nice = ["ugknbfddgicrmopn", "aaa", "jchzalrnumimnmhp", "haegwjzuvuyypxyu", "dvszwmarrgswjxmb"]

forbidden = ["ab", "cd", "pq", "xy"]
vowels_chars = "aeiou"
vowels_index = {
    "a": 0,
    "e": 1,
    "i": 2,
    "o": 3,
    "u": 4
}

for i in range(len(nice) - 1, -1, -1):
    # print(nice[i])

    vowels = [0, 0, 0, 0, 0]
    doubles = False
    forb = False 

    for nono in forbidden:
        if nono in nice[i]:
            forb = True

    for j in range(len(nice[i])):

        if nice[i][j] in vowels_chars:
            vowels[vowels_index[nice[i][j]]] += 1
        if j > 0 and nice[i][j] == nice[i][j - 1]:
            doubles = True
    
    # Rules
    if sum(vowels) < 3 or not doubles or forb:
        nice.pop(i)

print(len(nice))

###### PART 2 ######
with open("input.txt") as f:
    nice = f.read().splitlines()
# nice = ["qjhvhtzxzqqjkmpb", "xxyxx", "uurcxstgmygtbstg", "ieodomkazucvgmuy"]

for i in range(len(nice) - 1, -1, -1):
    # print(nice[i])

    # Rule 1
    duos = [nice[i][j] + nice[i][j + 1] for j in range(len(nice[i]) - 1)]
    duo = False
    for j in range(len(duos) - 1):
        for k in range(j + 2, len(duos)):
            if duos[j] == duos[k]:
                duo = True 
    # print(duo)

    # Rule 2
    repeat = False
    for j in range(len(nice[i]) - 2):
        if nice[i][j] == nice[i][j + 2]:
            repeat = True
    # print(repeat)

    if not duo or not repeat:
        nice.pop(i)

print(len(nice))
