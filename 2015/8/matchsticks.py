with open("input.txt") as f:
# with open("example.txt") as f:
    literals = f.read().splitlines()

code, mem = 0, 0
for literal in literals:
    code += len(literal)

    i = 1
    while i < len(literal) - 1:
        if literal[i] == '\\' and (literal[i + 1] == '"' or literal[i + 1] == '\\'):
            i += 1

        elif literal[i] == '\\' and literal[i + 1] == 'x':
            i += 3
        
        mem += 1
        i += 1

# Result part 1
print(code - mem)

###############

code, mem = 0, 0
for literal in literals:
    mem += len(literal)
    code += len(literal) + literal.count('\\') + literal.count('"') + 2

# Result part 2
print(code - mem)

