from collections import defaultdict

class Files:

    def __init__(self, file):
        with open(file) as f:
            self.data = f.read().splitlines()

    def crawl(self):
        open_folders = []
        folders = defaultdict(lambda: 0)

        for line in self.data:
            line = line.split()

            if line[0] == "dir" or (len(line) == 2 and line[0] == "$"):
                pass
            elif len(line) == 2:
                path = ""
                for folder in open_folders:
                    path += folder
                    folders[path] += int(line[0])
            elif line[2] == "..":
                open_folders.pop()
            elif line[1] == "cd":
                open_folders.append(line[2])
            else:
                print(line)

        return folders

    def sum_smallest(self, folders):
        return sum(size for size in folders.values() if size <= 100000)

    def to_remove(self, folders):
        sizes = sorted(list(folders.values()))
        for size in sizes:
            if 70000000 - sizes[-1] + size >= 30000000:
                return size


files = Files("example.txt")
files = Files("input.txt")

folders = files.crawl()
p1 = files.sum_smallest(folders)
p2 = files.to_remove(folders)
print(p1, p2)