class Device:

    def __init__(self, example, file):
        with open(example) as f:
            self.example = f.read().splitlines()
        with open(file) as f:
            self.data = f.read().splitlines()[0]

    def test(self):
        for line in self.example:
            mark = self.find(line)
            print(mark)

    def find(self, packet=None):
        if not packet:
            packet = self.data
        
        for i in range(len(packet) - 13):
            subset = set(packet[i : i + 14])
            if len(subset) == 14:
                return i + 14
        return -1
        




device = Device("example.txt", "input.txt")
device.test()
mark = device.find()
print(mark)