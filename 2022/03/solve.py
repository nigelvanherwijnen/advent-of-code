class Rucksacks:

    def __init__(self, file):
        with open(file) as f:
            self.sacks = f.read().splitlines()

    def errors(self):
        prios = 0
        for el in self.sacks:
            misplaced = self.common(el).pop()
            prios += self.get_prio(misplaced)
        return prios

    def common(self, sack):
        return set(sack[0:len(sack)//2]).intersection(set(sack[len(sack)//2::]))

    def get_prio(self, item):
        val = ord(item) - ord('A') + 1
        if val <= 26:
            val += 26
        else:
            val -= 26 + (ord('a') - ord('Z')) - 1
        return val

    def badges(self):
        prios = 0
        for i in range(0, len(self.sacks), 3):
            badge = set(self.sacks[i]).intersection(set(self.sacks[i + 1]))
            badge = badge.intersection(set(self.sacks[i + 2]))
            prios += self.get_prio(badge.pop())
        return prios
            
rucksack = Rucksacks("example.txt")
rucksack = Rucksacks("input.txt")
# prios = rucksack.errors()
# print(prios)

prios = rucksack.badges()
print(prios)