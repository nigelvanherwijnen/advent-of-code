class Cleaners:

    def __init__(self, file):
        with open(file) as f:
            self.pairs = f.read().splitlines()

    def full_overlap(self):
        overlaps = 0
        for pair in self.pairs:
            pair = pair.replace('-', ',').split(',')
            pair = [int(el) for el in pair]
            sign = (pair[0] - pair[2]) * (pair[3] - pair[1])
            if sign >= 0:
                overlaps += 1
        return overlaps

    def any_overlap(self):
        overlaps = len(self.pairs)
        for pair in self.pairs:
            pair = pair.replace('-', ',').split(',')
            pair = [int(el) for el in pair]

            if pair[0] < pair[2] and pair[1] < pair[2]:
                overlaps -= 1
            elif pair[0] > pair[3] and  pair[1] > pair[3]:
                overlaps -= 1
        return overlaps

cleaners = Cleaners("example.txt")
cleaners = Cleaners("input.txt")
full = cleaners.full_overlap()
all = cleaners.any_overlap()
print(full, all)