class Crane:

    def __init__(self, file):
        with open(file) as f:
            data = f.read().splitlines()
        # setup = data[0:3]
        # cols = 3
        # moves = data[5::]
        setup = data[0:8]
        cols = 9
        moves = data[10::]

        self.crates = [[] for i in range(cols)]
        for row in reversed(setup):
            for i in range(1, len(row), 4):
                if row[i].isalpha():
                    self.crates[i//4].append(row[i])

        moves = [el.replace("move", "").replace("from", "").replace("to", "").split() for el in moves]
        self.moves = [[int(row[0]), int(row[1]) - 1, int(row[2]) - 1] for row in moves]


    def move_all(self, version=9000):
        for row in self.moves:
            if version == 9000:
                self.move(*row)
            else:
                self.move_nein(*row)


    def move(self, amount, source, target):
        for i in range(amount):
            crate = self.crates[source].pop()
            self.crates[target].append(crate)

    
    def move_nein(self, amount, source, target):
        self.crates[target].extend(self.crates[source][-amount::])
        del self.crates[source][-amount::]

    def top_crates(self):
        for col in self.crates:
            if len(col) == 0:
                continue 
            print(col[-1], end='')
        print()


crane = Crane("example.txt")
crane = Crane("input.txt")
# crane.move_all()
# crane.top_crates()

crane.move_all(version=9001)
crane.top_crates()