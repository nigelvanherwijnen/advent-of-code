class Game:

    def __init__(self, file):
        with open(file) as f:
            self.strat = f.read().splitlines()

        self.strat = [el.split() for el in self.strat]
        for i, el in enumerate(self.strat):
            self.strat[i] = [ord(el[0]) - ord('@'), ord(el[1]) - ord('W')]
        # print(self.strat)

    def score(self):
        shape = sum([el[1] for el in self.strat])
        outcome = 0

        for el in self.strat:
            if el[0] == el[1]:
                outcome += 3
            elif (el[0] == 1 and el[1] == 2) or \
                    (el[0] == 2 and el[1] == 3) or \
                    (el[0] == 3 and el[1] == 1):
                outcome += 6

        return shape + outcome

    def predict(self):
        
        for i, el in enumerate(self.strat):
            if el[1] == 2:
                self.strat[i][1] = self.strat[i][0]
            elif el[1] == 1:
                self.strat[i][1] = self.strat[i][0] - 1 if self.strat[i][0] != 1 else 3
            elif el[1] == 3:
                self.strat[i][1] = self.strat[i][0] + 1 if self.strat[i][0] != 3 else 1

        return
    
game = Game("example.txt")
game = Game("input.txt")
game.predict()
score = game.score()
print(score)