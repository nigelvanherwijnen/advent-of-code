class Screen:

    def __init__(self, file):
        with open(file) as f:
            self.signals = f.read().splitlines()


    def render(self):
        cycle = 0
        position = 1
        screen = ""
        
        for signal in self.signals:
            cycle, pixel = self.write(cycle, position)
            screen += pixel
                
            signal = signal.split()
            if len(signal) == 1:
                continue 
            
            cycle, pixel = self.write(cycle, position)
            screen += pixel
            position += int(signal[1])
        return screen


    def write(self, cycle, position):
        cycle = cycle % 40
        cycle += 1
        pixel = "\n" if cycle == 1 else ""
        if abs(cycle - position - 1) <= 1:
            pixel += '#'
        else:
            pixel += ' '
        return cycle, pixel


# screen = Screen("mini.txt")
screen = Screen("example.txt")
screen = Screen("input.txt")
screen = screen.render()
print(screen)