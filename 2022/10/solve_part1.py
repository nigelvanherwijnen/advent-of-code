class Screen:

    def __init__(self, file):
        with open(file) as f:
            self.signals = f.read().splitlines()

    def calc_strength(self):
        strength = 1
        cycle = 0
        sum_strength = 0
        
        for signal in self.signals:
            cycle += 1
            if cycle in [20, 60, 100, 140, 180, 220]:
                sum_strength += cycle * strength     
                
            signal = signal.split()
            if len(signal) == 1:
                continue 
            
            cycle += 1
            if cycle in [20, 60, 100, 140, 180, 220]:
                sum_strength += cycle * strength     
            strength += int(signal[1])
        return sum_strength



            
# screen = Screen("mini.txt")
screen = Screen("example.txt")
screen = Screen("input.txt")
strength = screen.calc_strength()
print(strength)