elements = []
with open("input.txt", "r") as file:
    elements = file.read().splitlines()

sid = []
for el in elements:

    lrow, urow = 0, 127
    for i in range(7):
        middle = ((urow - lrow) // 2) + lrow
        if el[i] == "F":
            urow = middle
        else:
            lrow = middle + 1

    lcol, ucol = 0, 7
    for i in range(3):
        middle = ((ucol - lcol) // 2) + lcol
        if el[i + 7] == "L":
            ucol = middle
        else:
            lcol = middle + 1

    sid.append(lrow * 8 + lcol)
    

all_sid = []
for i in range(127):
    for j in range(8):
        all_sid.append(i * 8 + j)

els = set(sid).symmetric_difference(all_sid)
print(els)