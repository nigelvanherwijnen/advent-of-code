import numpy as np

elements = []
with open("input.txt", "r") as file:
    elements = file.read().splitlines()

mult = 1
for rshift, cshift in [[1, 1], [1, 3], [1, 5], [1, 7], [2, 1]]:
    trees = 0
    row, col, rows, cols = rshift, cshift, len(elements), len(elements[0])
    while row < rows:
        el = elements[row][col % cols]
        if el == "#":
            trees += 1

        row += rshift
        col += cshift
    mult *= trees

print(mult)