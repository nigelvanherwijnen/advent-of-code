import numpy as np
import copy

def print_grid(g):
    for line in g:
        print(line)

with open("input.txt", "r") as file:
    elements = file.read().splitlines()
elements = [list(el) for el in elements]
# grid = [np.empty((len(elements), len(elements[0]))), np.empty((len(elements), len(elements[0])))]
grid = [copy.deepcopy(elements), copy.deepcopy(elements)]
row_length = len(elements)
col_length = len(elements[0])
# print_grid(grid[0])
# print()

change = 1
t = 0
while change != 0:
    change = 0

    for row in range(row_length):
        for col in range(col_length):

            count = 0
            if grid[t % 2][row][col] == ".":
                continue

            for i in range(3):

                scan_row = row - 1 + i
                if scan_row < 0 or scan_row >= row_length:
                    continue

                for j in range(3):

                    scan_col = col - 1 + j
                    if scan_col < 0 or scan_col >= col_length:
                        continue
                    if scan_row == row and scan_col == col:
                        continue
                    if grid[t % 2][scan_row][scan_col] == "#":
                        count += 1

            if grid[t % 2][row][col] == "L" and count == 0:
                grid[(t + 1) % 2][row][col] = "#"
                change += 1
            elif grid[t % 2][row][col] == "#" and count >= 4:
                grid[(t + 1) % 2][row][col] = "L"
                change += 1
            else:
                grid[(t + 1) % 2][row][col] = grid[t % 2][row][col]

    t += 1

# print_grid(grid[(t + 1) % 2])

occupied = sum([1 for row in grid[(t + 1) % 2] for c in row if c == "#"])
print(occupied)