import numpy as np
import copy

def print_grid(g):
    for line in g:
        print(line)

with open("input.txt", "r") as file:
    elements = file.read().splitlines()
elements = [list(el) for el in elements]
# grid = [np.empty((len(elements), len(elements[0]))), np.empty((len(elements), len(elements[0])))]
grid = [copy.deepcopy(elements), copy.deepcopy(elements)]
row_length = len(elements)
col_length = len(elements[0])
# print_grid(grid[0])
# print()

change = 1
t = 0
while change != 0:
    change = 0

    for row in range(row_length):
        for col in range(col_length):

            count = [[".", "I", "I"], ["I", "I", "I"], ["I", "I", "I"]]
            if grid[t % 2][row][col] == ".":
                continue

            d = 0
            while count[0].count(".") > 0 or count[1].count(".") > 0 or count[2].count(".") > 0:
                if d == 0:
                    count[0][0] = "I"
                d += 1
                for i, scan_row in enumerate([row - d, row, row + d]):
                    for j, scan_col in enumerate([col - d, col, col + d]):

                        if scan_row < 0 or scan_row >= row_length:
                            if count[i][j] == ".":
                                count[i][j] = "I"
                            continue
                        if scan_col < 0 or scan_col >= col_length:
                            if count[i][j] == ".":
                                count[i][j] = "I"
                            continue
                        if scan_row == row and scan_col == col:
                            count[i][j] = "I"
                            continue
                        if count[i][j] != "." and count[i][j] != "I":
                            continue

                        count[i][j] = grid[t % 2][scan_row][scan_col]

            count = [c for row in count for c in row]

            if grid[t % 2][row][col] == "L" and count.count("#") == 0:
                grid[(t + 1) % 2][row][col] = "#"
                change += 1
            elif grid[t % 2][row][col] == "#" and count.count("#") >= 5:
                grid[(t + 1) % 2][row][col] = "L"
                change += 1
            else:
                grid[(t + 1) % 2][row][col] = grid[t % 2][row][col]

    t += 1
    # break

# print_grid(grid[(t + 1) % 2])
# print_grid(grid[0])
# print()
# print_grid(grid[1])

occupied = sum([1 for row in grid[(t + 1) % 2] for c in row if c == "#"])
print(occupied)