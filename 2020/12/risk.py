import numpy as np
import copy

def change_directions(comm, d):
    R = np.array([[0, -1], [1, 0]])
    if comm[0] == "L":
        R = -R
    for i in range(int(comm[1::]) // 90):
        d = np.dot(d, R)
    return d

def step(pos, direc, amount):
    return pos + direc * amount

def get_dir(move):
    if move == "N":
        return np.array([0, 1])
    if move == "S":
        return np.array([0, -1])
    if move == "E":
        return np.array([1, 0])
    return np.array([-1, 0])

with open("input.txt", "r") as file:
    elements = file.read().splitlines()

directions = np.array([10, 1])
pos = np.array([0, 0])
for el in elements:
    print(pos, directions)

    if el[0] == "R" or el[0] == "L":
        directions = change_directions(el, directions)
        continue

    if el[0] == "F":
        pos = step(pos, directions, int(el[1::]))
        continue

    directions = step(directions, get_dir(el[0]), int(el[1::]))

print(pos)
print(np.sum(np.abs(pos)))