import numpy as np 

elements = []
with open("input.txt", "r") as file:
    elements = file.read().splitlines()

valid = 0
for el in elements:
    limits, character, phrase = el.split(" ")
    limits = [int(i) for i in limits.split("-")]
    character = character.strip(":")
    count = phrase.count(character)
    # print(limits, character, phrase, count)

    if count >= limits[0] and count <= limits[1]:
        valid += 1

print(valid)