import numpy as np 

elements = []
with open("input.txt", "r") as file:
    elements = file.read().splitlines()

valid = 0
for el in elements:
    limits, character, phrase = el.split(" ")
    limits = [int(i) for i in limits.split("-")]
    character = character.strip(":")

    if (phrase[limits[0] - 1] == character) != (phrase[limits[1] - 1] == character):
        valid += 1

print(valid)