with open("input.txt", "r") as file:
    elements = file.read().splitlines()
elements = [int(el) for el in elements]
    
# print(elements)

pre = 25
for i in range(pre, len(elements)):
    check = False
    for j in range(pre - 1):
        el = elements[i - pre + j]
        sums = [el + s for s in elements[i - pre + j + 1:i]]
        if elements[i] in sums:
            check = True
            break
    if check == False:
        print(elements[i])
        target = elements[i]
        break
        
comb = []
for i in range(len(elements)):
    j = i
    while sum(comb) < target:
        comb.append(elements[j])
        j += 1
    if sum(comb) == target:
        print(comb)
        break
    else:
        comb = []
        
print(min(comb) + max(comb))