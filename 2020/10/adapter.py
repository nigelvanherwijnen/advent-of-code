import copy
import numpy as np

elements = []
with open("input.txt", "r") as file:
    elements = file.read().splitlines()
elements = [0] + [int(el) for el in elements]
elements = sorted(elements)
# print(elements)

""" Part 1 """
diffs = [0, 0, 1]
diffs[elements[1] - 1] += 1
for i in range(1, len(elements)):
    diff = elements[i] - elements[i - 1]
    diffs[diff - 1] += 1
print(diffs[0] * diffs[2])

""" Part 2 """
solutions = np.zeros(len(elements))
solutions[-1] = 1
for i in range(len(elements) - 2, -1, -1):

    els = [elements[i]]
    for j in range(1, 4):
        if i + j >= len(elements):
            continue
        if elements[i + j] -  elements[i] <= 3:
            solutions[i] += solutions[i + j]
            els.append(elements[i + j])

print(int(solutions[0]))
    
