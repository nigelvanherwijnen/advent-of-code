import numpy as np

""" Part 1 """
# with open("input.txt", "r") as file:
#     elements = file.read().splitlines()

# start_time = int(elements[0])
# busses = elements[1]
# busses = np.array([int(el) for el in busses.replace(",x", "").split(",")])

# times = np.zeros(len(busses))
# for i, t in enumerate(busses):
#     while times[i] < start_time:
#         times[i] += busses[i]
# print(busses[np.argmin(times)] * (np.min(times) - start_time))

""" Part 2 """
# def check(t):
#     for i in range(1, len(t)):
#         if t[i] != t[i - 1] + 1:
#             return False
#     return True

# with open("input.txt", "r") as file:
#     elements = file.read().splitlines()

# busses = elements[1]
# busses = [el if el == 'x' else int(el) for el in busses.split(",")]
# times = np.zeros(len(busses))
# cur_max = len(busses)

# for i in range(len(busses)):
#     if busses[i] == "x":
#         times[i] = times[i - 1] + 1
#         continue
#     times[i] = (100000000000000 // busses[i]) * busses[i]

# while not check(times):
#     times[0] += busses[0]
#     for cur in range(1, cur_max):

#         if busses[cur] == "x":
#             times[cur] = times[cur - 1] + 1
#             continue

#         while times[cur] <= times[cur - 1]:
#             times[cur] += busses[cur]
# print(times)

with open("input.txt", "r") as file:
    elements = file.read().splitlines()

busses = elements[1]
busses = [el if el == 'x' else int(el) for el in busses.split(",")]
lb = len(busses)

times = np.zeros(lb)
# time = (100000000000000 // busses[0]) * busses[0]
time = 0
# print(time)

incorrect = True
while incorrect:
    time += busses[0]
    incorrect = False

    for i in range(1, lb):
        if busses[i] == "x":
            continue
        diff = (time + i) / busses[i]
        
        if not diff.is_integer():
            incorrect = True
            break
    
# print(time)


for i in range(lb):
    if busses[i] == "x":
        continue
    print(busses[i], i)

