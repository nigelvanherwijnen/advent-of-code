import numpy as np

# byr (Birth Year)
# iyr (Issue Year)
# eyr (Expiration Year)
# hgt (Height)
# hcl (Hair Color)
# ecl (Eye Color)
# pid (Passport ID)
# cid (Country ID)
fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

elements = []
with open("input.txt", "r") as file:
    elements = file.read().splitlines()

elements = elements + [""]

valid = 0
count = 0
for el in elements:
    
    cur = [1 if field in el else 0 for field in fields]
    count += sum(cur)
    
    if len(el) == 0:
        valid = valid + 1 if count == 7 else valid
        count = 0
print(valid)
        
    

