import re

# byr (Birth Year)
# iyr (Issue Year)
# eyr (Expiration Year)
# hgt (Height)
# hcl (Hair Color)
# ecl (Eye Color)
# pid (Passport ID)
# cid (Country ID)
fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

# Load
elements = []
with open("input.txt", "r") as file:
    elements = file.read().splitlines()

# Combine passport into single elements
elements = elements + [""]
stacked = []
cur = ""
for el in elements:
    cur = cur + " " + el
    if len(el) == 0:
        stacked.append(cur)
        cur = ""

# Check each passport
valid = 0
for el in stacked:

    byr = re.search(r"\bbyr:\w+", el)
    iyr = re.search(r"\biyr:\w+", el)
    eyr = re.search(r"\beyr:\w+", el)
    hgt = re.search(r"\bhgt:\w+", el)
    hcl = re.search(r"\bhcl:#\w+", el)
    ecl = re.search(r"\becl:\w+", el)
    pid = re.search(r"\bpid:\w+", el)
    if not byr or not iyr or not eyr or not hgt or not hcl or not ecl or not pid:
        continue

    y = int(byr.group()[4::])
    if y < 1920 or y > 2002:
        continue
    
    y = int(iyr.group()[4::])
    if y < 2010 or y > 2020:
        continue

    y = int(eyr.group()[4::])
    if y < 2020 or y > 2030:
        continue
    
    h = int(hgt.group().split(":")[-1].strip("cm").strip("in"))
    if hgt.group()[-2::] == "cm" and h >= 150 and h <= 193:
        pass
    elif hgt.group()[-2::] == "in" and h >= 59 and h <= 76:
        pass
    else:
        continue

    c = hcl.group().split("#")[-1]
    if re.search(r"[G-Zg-z]", c):
        continue

    if not re.search(ecl.group().split(":")[-1], "amb blu brn gry grn hzl oth"):
        continue

    if len(pid.group().split(":")[-1]) != 9:
        continue

    valid += 1
print(valid)


        
    

