elements = []
with open("input.txt", "r") as file:
    elements = file.read().splitlines()

elements = [el.split(" ") for el in elements]
elements = [[el[0], int(el[1])] for el in elements]

def perform(elements):

    acc = 0
    line = 0
    lines = []  
    while line < len(elements):

        if lines.count(line) == 2:
            return False, 0, lines
        lines.append(line)

        if elements[line][0] == "jmp":
            line += elements[line][1] - 1

        elif elements[line][0] == "acc":
            acc += elements[line][1]

        line += 1

    return True, acc, lines

_, _, lines = perform(elements)
nop = [i for i in lines if elements[i][0] == "nop"]
jmp = [i for i in lines if elements[i][0] == "jmp"]

for j in jmp:
    elements[j][0] = "nop"
    success, accum, _ = perform(elements)
    elements[j][0] = "jmp"

    if success:
        print(accum)

