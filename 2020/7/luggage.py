elements = []
with open("input.txt", "r") as file:
    elements = file.read().splitlines()

""" Part 1 """
# containers = dict()
# for el in elements:
#     outer, inner = el.split(" bags contain ")
#     inner = inner.strip(".").split(",")
    
#     for b in inner:
#         b = b.strip("bags").strip(" ")
#         if b[0:2] == "no":
#             continue

#         if b[2::] not in containers.keys():
#             containers[b[2::]] = [outer]
#         else:
#             containers[b[2::]].append(outer)

# unique = []
# queue = containers["shiny gold"]
# for el in queue:
#     if el in unique:
#         continue
#     unique.append(el)
#     if el not in containers.keys():
#         continue
#     for new in containers[el]:
#         queue.append(new)
# print(len(unique))

""" Part 2 """
containers = dict()
for el in elements:
    outer, inner = el.split(" bags contain ")
    inner = inner.strip(".").split(",")

    for bag in inner:
        bag = bag.strip("bags").strip(" ")
        if outer not in containers.keys():
            containers[outer] = []
        if bag[0:2] == "no":
            continue
        containers[outer].append({"bag": bag[2::], "n":int(bag.split(" ")[0])})

def count(cont, name):
    amount = 0
    for el in cont[name]:
        amount += el["n"] * (count(cont, el["bag"]) + 1)
    return amount

print(count(containers, "shiny gold"))