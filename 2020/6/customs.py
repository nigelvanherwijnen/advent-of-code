elements = []
with open("input.txt", "r") as file:
    elements = file.read().splitlines()
elements.append("")

""" Part 1 """
count = 0
cur = ""
for el in elements:
    if el == "":
        count += len(set(cur))
        cur = ""
    cur += el
print(count)

""" Part 2 """
count = 0
lines = -1
cur = ""
for el in elements:
    cur += el
    lines += 1
    if el == "":
        for char in set(cur):
            if cur.count(char) // lines == 1:
                count += 1
        cur = ""
        lines = -1
print(count)