from pprint import pprint 
class Crabs:

    def __init__(self, file):

        with open(file) as f:
            positions = f.read().splitlines()
        positions = [int(el) for el in positions[0].split(",")]

        self.crab_dist = dict()
        for pos in positions:
            if int(pos) in self.crab_dist.keys():
                self.crab_dist[int(pos)] += 1
            else:
                self.crab_dist[int(pos)] = 1
        self.width = max(self.crab_dist.keys())

        self.distances = self.calc_distances()
        

    def calc_distances(self, constant=True):
        constant=False

        distances = dict()
        for i in range(self.width + 1):
            distances[i] = dict()
            for j in range(self.width + 1):
                distances[i][j] = abs(i - j)

                if not constant:
                    distances[i][j] = sum(range(distances[i][j] + 1))
        return distances


    def align(self):

        min_fuel = self.width ** 2 * len(self.crab_dist)
        for i in range(self.width + 1):

            fuel = 0
            for pos, crabs in self.crab_dist.items():
                fuel += self.distances[i][pos] * crabs 
            min_fuel = fuel if fuel < min_fuel else min_fuel

        print(min_fuel)


crabs = Crabs("example.txt")
crabs = Crabs("input.txt")
crabs.align()