class Submarine:

    def __init__(self, file):
    
        with open(file) as f:
            self.directions = f.read().splitlines()
            
        self.loc = [0, 0, 0]
        self.axes = {
            "forward": [0, 1],
            "backward": [0, -1],
            "up": [2, -1],
            "down": [2, 1]
        }

    def move(self, direction, amount):

        axis, mult = self.axes[direction]
        self.loc[axis] += amount * mult 

        if axis == 0:
            self.loc[1] += self.loc[2] * amount * mult

    def full_dive(self):

        for direction in self.directions:
            axis, amount = direction.split()
            self.move(axis, int(amount))

    def answer(self, i):
        return self.loc[0] * self.loc[2 if i == 1 else 1]

    
sub = Submarine("example.txt")
sub = Submarine("input.txt")
sub.full_dive()
print(sub.answer(1))
print(sub.answer(2))