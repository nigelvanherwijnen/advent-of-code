class School:

    def __init__(self, input):

        # Read input
        with open(input) as f:
            timers = f.read().split(",")
        
        # Init state
        self.day = 0
        self.max_day, self.new_day = 6, 8
        self.state = [0 for i in range(self.new_day + 1)]
        for timer in timers:
            self.state[int(timer)] += 1


    def pass_day(self):

        # Stage new fish
        temp = self.state[-2]
        self.state[-2] = self.state[-1]
        self.state[-1] = self.state[self.day % (self.max_day + 1)]

        # Introduce new fish into cycle
        self.state[self.day % (self.max_day + 1)] += temp
        self.day += 1

    def count_fish(self):
        return sum(self.state)


school = School("example.txt")
school = School("input.txt")
for i in range(256):
    school.pass_day()
print(school.count_fish())







# Different method

# class School:

#     def __init__(self, input):

#         # Read input
#         with open(input) as f:
#             timers = f.read().split(",")
        
#         # Empty state holder
#         self.min_day, self.max_day, self.new_day = 0, 6, 8
#         self.state = [dict(), dict()]
#         for i in range(2):
#             for j in range(self.min_day, self.new_day + 1):
#                 self.state[i][j] = 0

#         # Fill initial state
#         self.day = 0
#         for timer in timers:
#             self.state[self.day][int(timer)] += 1


#     def pass_day(self):

#         # Regular cycle
#         self.day += 1
#         for i in range(self.min_day, self.max_day + 1):
#             self.state[self.day % 2][i] = self.state[(self.day + 1) % 2][(i + 1) % (self.max_day + 1)]

#         # New in regular cycle
#         self.state[self.day % 2][self.max_day] += self.state[(self.day + 1) % 2][self.max_day + 1]

#         # Newborns
#         for i in range(self.max_day + 1, self.new_day + 1):
#             self.state[self.day % 2][i] = self.state[(self.day + 1) % 2][(i + 1) % (self.new_day + 1)]


#     def count_fish(self):
#         return sum(self.state[self.day % 2].values())