class Basin:

    def __init__(self, filename):

        with open(filename) as f:
            self.basin = f.read().splitlines()
        self.basin = [[int(cell) for cell in row] for row in self.basin]
        # print(self.basin)


    def find_paths(self):

        self.paths = dict()
        for i in range(len(self.basin)):
            for j in range(len(self.basin[0])):
                # print(self.basin[i][j])

                current = self.basin[i][j]
                lowest, coord = self.find_lower((i, j))
                
                self.paths[(i, j)] = coord if lowest < current else None
        # print(self.paths)

    
    def find_lower(self, start):

        current = self.basin[start[0]][start[1]]
        lowest, coord = 10, (0, 0)
        for i in [start[0] - 1, start[0], start[0] + 1]:
            for j in [start[1] - 1, start[1], start[1] + 1]:
                if i < 0 or j < 0 or \
                        i >= len(self.basin) or j >= len(self.basin[0]) or \
                        (start[0]  == i and start[1] == j) or \
                        (start[0]  != i and start[1] != j):
                    continue 
                if self.basin[i][j] < lowest:
                    lowest = self.basin[i][j]
                    coord = (i, j)
        
        return lowest, coord


    def loop_risk_levels(self):

        risk = 0
        for source, target in self.paths.items():
            if target is None:
                risk += self.basin[source[0]][source[1]] + 1
        return risk


    def find_lowest(self, coord):

        if self.paths[coord] is None:
            return coord 
        return self.find_lowest(self.paths[coord])


    def find_bassins(self):

        bassins = dict()
        for coord in self.paths.keys():
            if self.basin[coord[0]][coord[1]] == 9:
                continue
            lowest = self.find_lowest(coord)
            # print(lowest, coord)
            if lowest in bassins.keys():
                bassins[lowest] += 1
            else:
                bassins[lowest] = 1
        # print(bassins)
        sizes = sorted(list(bassins.values()), reverse=True)
        return sizes[0] * sizes[1] * sizes[2]


basin = Basin("example.txt")
basin = Basin("input.txt")

basin.find_paths()
risk = basin.loop_risk_levels()
print(risk)
bassins = basin.find_bassins()
print(bassins)