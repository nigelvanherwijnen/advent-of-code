class Submarine:

    def __init__(self, filename):

        with open(filename) as f:
            self.chunks = f.read().splitlines()

        self.points = {
            ")": 3,
            "]": 57,
            "}": 1197,
            ">": 25137
        }
        self.points_complete = {
            ")": 1,
            "]": 2,
            "}": 3,
            ">": 4
        }

        self.open = {"(": ")", "[": "]", "{": "}", "<": ">"}


    def check_lines(self):

        points = 0
        points_auto = []
        for chunk in self.chunks:
            error = False
            queue = []
            for char in chunk:

                if char in self.open.keys():
                    queue.append(self.open[char])
                elif (close := queue.pop()) is not char:
                        points += self.points[char]
                        error = True 
                        break
                
            if error == True:
                continue
            
            points_auto.append(0)
            while len(queue):
                char = queue.pop()
                points_auto[-1] *= 5
                points_auto[-1] += self.points_complete[char]


        points_auto = sorted(points_auto)
        print(points)
        print(points_auto[len(points_auto) // 2])

    
sub = Submarine("example.txt")    
sub = Submarine("input.txt")

sub.check_lines()
