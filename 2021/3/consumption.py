class Submarine:

    def __init__(self, file):
    
        with open(file) as f:
            self.report = f.read().splitlines()
            self.lines = len(self.report)
            self.line_size = len(self.report[0])

    
    def calc_power(self):

        gamma_eps = [0, 0]
        for i in range(self.line_size - 1, -1, -1):
            count = 0
            for j in range(self.lines):
                count += int(self.report[j][i])

            gamma_eps[round(count / self.lines)] += 2 ** (self.line_size - i - 1)

        return gamma_eps[0] * gamma_eps[1]


    def calc_life(self):

        skip_ox, skip_scrub = [], []
        oxygen, scrubber = range(self.lines), range(self.lines)
        for i in range(self.line_size):

            ind_ox, ind_scrub = [[], []], [[], []]
            for j in oxygen:
                ind_ox[int(self.report[j][i])].append(j)
            for j in scrubber:
                ind_scrub[int(self.report[j][i])].append(j)

            if len(oxygen) != 1:
                skip_ox += ind_ox[int(len(ind_ox[0]) > len(ind_ox[1]))]
            if len(scrubber) != 1:
                skip_scrub += ind_scrub[int(len(ind_scrub[0]) <= len(ind_scrub[1]))]

            oxygen = [j for j in range(self.lines) if j not in skip_ox]
            scrubber = [j for j in range(self.lines) if j not in skip_scrub]

        ox, scrub = 0, 0
        for i in range(self.line_size - 1, -1, -1):
            ox += int(self.report[oxygen[0]][i]) * 2 ** (self.line_size - i - 1)
            scrub += int(self.report[scrubber[0]][i]) * 2 ** (self.line_size - i - 1)

        return ox * scrub


    def calc_life_recur(self):

        oxygen = self.get_rating(self.report)[0]
        scrubber = self.get_rating(self.report, ox=False)[0]

        ox, scrub = 0, 0
        for i in range(self.line_size - 1, -1, -1):
            ox += int(oxygen[i]) * 2 ** (self.line_size - i - 1)
            scrub += int(scrubber[i]) * 2 ** (self.line_size - i - 1)

        return ox * scrub


    def get_rating(self, lines, bit=0, ox=True):

        if len(lines) == 1 or bit == len(lines[0]):
            return lines 

        split = [[], []]
        for i in range(len(lines)):
            split[int(lines[i][bit])].append(lines[i])

        if ox:
            lines = split[0] if len(split[0]) > len(split[1]) else split[1]
        else:
            lines = split[0] if len(split[0]) <= len(split[1]) else split[1]

        return self.get_rating(lines, bit + 1, ox=ox)


sub = Submarine("example.txt")
sub = Submarine("input.txt")
print(sub.calc_power())
print(sub.calc_life())
print(sub.calc_life_recur())
# print(sub.calc_life(sub.report))
