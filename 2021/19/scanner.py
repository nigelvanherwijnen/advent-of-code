from pprint import pprint
ND = 3
N_CHECK = 12

class Search:

    def __init__(self, filename): 

        with open(filename) as f:
            scanners = f.read().split("\n\n")
        self.scanners = [Scanner(scanner) for scanner in scanners]
        self.beacons = set()
        self.origins = set()


    def init_scanner(self):
        scanner = self.scanners.pop(0)
        self.origins.add(scanner.get_origin())
        for beacon in scanner.get_coords():
            self.beacons.add(beacon)
        # print(self.origins)
        # print(self.beacons)


    
    def analyze_scanner(self, scanner):
        print()
        print(scanner)

        # Loop over possible configurations
        new_beacons = scanner.get_coords()
        for i, coord_new in enumerate(new_beacons):
            for coord_known in self.beacons:
                # print(coord_new, 'joe')

                # Transpose / rotate
                # print(coord_known, coord_new, shift)
                for direc in [["x", "y"], ["z", "y"], ["x", "y"], ["z", "x"], ["y", "x"], ["z", "x"], ["y", "x"]]:
                    for k in range(4):
                        scanner.rotate(direc[0])
                        shift = [coord_known[l] - new_beacons[i][l] for l in range(ND)]
                        scanner.transpose(shift)
                        
                        # Check
                        if self.check_overlap(scanner):
                            self.origins.add(scanner.get_origin())
                            for coord in new_beacons:
                                self.beacons.add(coord)
                            # print(new_beacons)
                            return
                        
                        # Reverse if not valid
                        scanner.transpose(shift, sign=-1)

                    scanner.rotate(direc[1])


        # Store for later if not yet possible
        self.scanners.append(scanner)


    def check_overlap(self, scanner):

        count = 0
        for check in scanner.get_coords():
            org = scanner.get_origin()
            for og in self.origins:
                if not (abs(org[0] - og[0]) < 1000 and abs(org[1] - og[1]) < 1000 and abs(org[2] - og[2]) < 1000):
                    continue
            if check in self.beacons:
                count += 1
            if count == N_CHECK:
                print("HEUJ", scanner.get_origin())
                return True 
        return False


class Scanner:

    def __init__(self, info):
        info = info.split("\n")
        # print(info)
        self.id = int(info[0].split(" ")[-2])
        self.coords = []
        self.origin = (0, 0, 0)
        for coord in info[1::]:
            self.coords.append(tuple((int(x) for x in coord.split(","))))
        self.n_beacons = len(self.coords)
        # print(self.coords)

    def __repr__(self):
        return f"Scanner {self.id}"

    def transpose(self, shift, sign=1):
        self.origin = tuple(self.origin[i] + (sign * shift[i]) for i in range(ND))
        for i in range(self.n_beacons):
            self.coords[i] = tuple(self.coords[i][j] + (sign * shift[j]) for j in range(ND))
        # print(self.coords)

    def rotate(self, axis, sign=1):
        M = {
            # "x": [[1, 0, 0], [0, 0, -1], [0, 1, 0]],
            # "y": [[0, 0, 1], [0, 1, 0], [-1, 0, 0]],
            # "z": [[0, -1, 0], [1, 0, 0], [0, 0, 1]]
            "x": [[1, 0, 0], [0, 0, 1], [0, -1, 0]],
            "y": [[0, 0, -1], [0, 1, 0], [1, 0, 0]],
            "z": [[0, 1, 0], [-1, 0, 0], [0, 0, 1]]
        }
        
        self.origin = tuple(self.multiply(self.origin, M[axis]))
        # print(self.origin)
        # self.origin = tuple(self.origin[i] * (sign * M[axis][i]) for i in range(ND))
        for i in range(self.n_beacons):
            # self.coords[i] = tuple(self.coords[i][j] * (sign * M[axis][j]) for j in range(ND))
            self.coords[i] = tuple(self.multiply(self.coords[i], M[axis]))

    def multiply(self, x, M):
        result = []
        for i in range(len(M[0])):
            combined = 0
            for j in range(len(x)):
                combined += x[j] * M[j][i]
            result.append(combined)
        return result


    def get_coords(self):
        return self.coords

    def get_n_beacons(self):
        return self.n_beacons

    def get_origin(self):
        return self.origin





search = Search("input.txt")
# search = Search("example.txt")
# search = Search("example_orientation.txt")
# search = Search("example_mini.txt")

search.init_scanner()
print(len(search.beacons))

for i in range(15):
    scanner = search.scanners.pop(0)
    search.scanners.append(scanner)
while len(search.scanners) > 0:
    
    scanner = search.scanners.pop(0)
    search.analyze_scanner(scanner)
    print(len(search.beacons))
    pprint(search.scanners)
    # pprint(search.origins)

longest = 0
for og in search.origins:
    for org in search.origins:
        dist = abs(org[0] - og[0]) + abs(org[1] - og[1]) + abs(org[2] - og[2])
        if dist > longest:
            longest = dist 
print(longest)