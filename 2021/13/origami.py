class Puzzle:

    def __init__(self, filename):

        with open(filename) as f:
           dots, folds = f.read().split("\n\n")
        self.dots = [tuple(int(i) for i in dot.split(",")) for dot in dots.split("\n")]
        self.folds = folds.split("\n")


    def fold_all(self):

        for fold in self.folds:
            fold = fold.split("=")
            ind = 0 if fold[0][-1] == "x" else 1
            val = int(fold[1])

            self.fold(ind, val)
            # break

        print(len(set(self.dots)))


    def fold(self, ind, val):
        
        for i in range(len(self.dots)):
            if self.dots[i][ind] > val:
                dot = list(self.dots[i])
                dot[ind] = val - (self.dots[i][ind] - val)
                self.dots[i] = tuple(dot)

        return


    def print_dots(self):

        dots = set(self.dots)
        xmax = 80
        ymax = 10
        for y in range(ymax):
            for x in range(xmax):
                if (x, y) in dots:
                    print("#", end="")
                else:
                    print(" ", end="")
            print()


puzzle = Puzzle("example.txt")
puzzle = Puzzle("input.txt")

puzzle.fold_all()
puzzle.print_dots()