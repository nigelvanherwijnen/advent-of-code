from pprint import pprint

class Polymere:

    def __init__(self, filename):

        with open(filename) as f:
            template = f.read().splitlines()

        rules = dict()
        self.reversed_rules = dict()
        self.polymere = [dict(), dict()]
        for rule in template[2:]:
            rule = rule.split(" -> ")

            self.polymere[0][rule[0]] = 0
            self.polymere[1][rule[0]] = 0
            rules[rule[0]] = [rule[0][0] + rule[1], rule[1] + rule[0][1]]
            self.reversed_rules[rule[0]] = []

        for source, target in rules.items():
            for pair in target:
                self.reversed_rules[pair].append(source)

        self.t = 0
        self.start = template[0]
        for i in range(len(self.start) - 1):
            self.polymere[self.t][self.start[i:i+2]] += 1


    def step(self):

        self.t += 1
        for target, source in self.reversed_rules.items():

            self.polymere[self.t % 2][target] = 0
            for pair in source:
                self.polymere[self.t % 2][target] += self.polymere[(self.t - 1) % 2][pair]

    
    def calc_answer(self):

        sol = dict()
        for key in self.reversed_rules:
            for ch in key:
                sol[ch] = 0

        for pair, amount in self.polymere[self.t % 2].items():
            for ch in pair:
                sol[ch] += amount
                
        sol[self.start[0]] += 1
        sol[self.start[-1]] += 1
        for key in sol:
            sol[key] //= 2

        return max(sol.values()) - min(sol.values())
        


poly = Polymere("example.txt")
poly = Polymere("input.txt")

for i in range(40):
    poly.step()
    print(poly.calc_answer())