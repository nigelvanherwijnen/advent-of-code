from pprint import pprint
class Paths:

    def __init__(self, filename):
        
        with open(filename) as f:
            paths_list = f.read().splitlines()

        self.locations = dict()
        for path in paths_list:
            path = path.split("-")
            
            for i in range(2):
                if path[i] not in self.locations.keys():
                    self.locations[path[i]] = Location(path[i])

            self.locations[path[0]].add_connection(self.locations[path[1]])
            self.locations[path[1]].add_connection(self.locations[path[0]])


    def search_paths(self, location=None, paths=[], current=[]):

        # Start or new location in path
        if location == None:
            location = self.locations["start"]
        current.append(location)

        # Check end of line
        if location.name == "end":
            paths.append(current)
            return paths, current[0:-1]

        # Loop over possible connections
        connections = location.get_connections()
        for connection in connections:

            if self.check_valid(connection, current, part2=True):
                paths, current = self.search_paths(location=connection, paths=paths, current=current)

        return paths, current[0:-1]


    def check_valid(self, location, path, part2=False):
        # Part 1
        if not part2:
            if location in path and not location.is_large():
                return False
            return True

        # Part 2
        if location.name == "start":
            return False
        if location.is_large() or location not in path:
            return True 
        lower = [loc for loc in path if not loc.is_large()]
        if len(lower) == len(set(lower)):
            return True 
        return False
        


class Location:

    def __init__(self, name):
        self.name = name 
        self.connected = []
        self.large = name.isupper()

    def __repr__(self):
        return f"{self.name}"

    def __str__(self):
        return f"location {self.name}"

    def add_connection(self, loc):
        self.connected.append(loc) 

    def get_connections(self):
        return self.connected 

    def is_large(self):
        return self.large



paths = Paths("example0.txt")
paths = Paths("example1.txt")
paths = Paths("example2.txt")
paths = Paths("input.txt")

all_paths = paths.search_paths()
pprint(all_paths)
print(len(all_paths[0]))