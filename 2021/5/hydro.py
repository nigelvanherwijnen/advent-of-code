class Submarine:

    def __init__(self, input):

        with open(input) as f:
            self.vents = f.read().splitlines()

        self.lines = dict()
        self.grid = [[0 for i in range(1000)] for j in range(1000)]


    def define_line(self, direction):

        source, target = direction.split(" -> ")

        source = [int(el) for el in source.split(",")]
        target = [int(el) for el in target.split(",")]

        steps = max([abs(source[0] - target[0]), abs(source[1] - target[1])])
        xslope = 1 if target[0] > source[0] else 0 if target[0] == source[0] else -1
        yslope = 1 if target[1] > source[1] else 0 if target[1] == source[1] else -1

        coords = [source]
        for i in range(steps):
            coords.append([coords[-1][0] + xslope, coords[-1][1] + yslope])

        return coords


    def add_lines(self):

        for i, direction in enumerate(self.vents):

            new_coords = self.define_line(direction)
            for coord in new_coords:
                self.grid[coord[0]][coord[1]] += 1 



    def count_doubles(self):

        count = 0
        for i in range(len(self.grid)):
            for j in range(len(self.grid[0])):
                if self.grid[i][j] > 1:
                    count += 1
        return count
        

sub = Submarine("example.txt")
sub = Submarine("input.txt")
sub.add_lines()
ans = sub.count_doubles()
print(ans)

