from pprint import pprint
class Submarine:

    def __init__(self, filename):
        
        with open(filename) as f:
            self.grid = [[int(cell) for cell in row] for row in f.read().splitlines()]
        self.size = len(self.grid)
        self.thresh = 9

        # pprint(self.grid)
        # print(self.size)

    
    def flash(self, x, y, flashed):

        if self.grid[x][y] <= self.thresh or (x, y) in flashed:
            return flashed
        flashed.append((x, y))

        for i in range(x - 1, x + 2):
            for j in range(y - 1, y + 2):

                if (not self.check_bounds(i, j)) or \
                    (x == i and y == j):
                    continue

                self.grid[i][j] += 1
                if self.grid[i][j] > self.thresh and (i, j) not in flashed:
                    flashed = self.flash(i, j, flashed)

        return flashed


    def check_bounds(self, x, y):
        return x >= 0 and y >= 0 and x < self.size and y < self.size


    def total(self):
        return sum([sum(row) for row in self.grid])

        
    def step(self):

        # Increment all
        for i in range(self.size):
            for j in range(self.size):
                self.grid[i][j] += 1

        # Flash octos
        flashed = []
        for i in range(self.size):
            for j in range(self.size):
                # if i != 1 or j != 1:
                #     continue
                flashed = self.flash(i, j, flashed)

        # Reset flashed falues
        for coord in flashed:
            self.grid[coord[0]][coord[1]] = 0
        
        pprint(self.grid)
        print()

        return len(flashed)




sub = Submarine("example.txt")
# sub = Submarine("example0.txt")
sub = Submarine("input.txt")

# nflash = 0
# for i in range(100):
#     nflash += sub.step()
# print(nflash)

step = 0
while sub.total() > 0:
    sub.step()
    step += 1
print(step)