class Submarine:

    def __init__(self, input):

        with open(input) as f:
            self.draws = f.readline().strip().split(',')
            self.boards = f.read().splitlines()#[1:]
            self.n_boards = len(self.boards)
            self.n_cols = len(self.boards[1].split())
            self.n_chars = len(self.boards[1])


    def print_boards(self):
        print("----")
        for row in self.boards:
            print(row)
        print("----")


    def draw(self):

        number = self.draws.pop(0)
        print(number)
        for i in range(self.n_boards):
            if " " + number in self.boards[i]:
                self.boards[i] = self.boards[i].replace(" " + number, " " * (len(number) + 1))
        return int(number)


    def check_win(self):

        self.print_boards()
        score, win = 0, False
        for i in range(1, self.n_boards, self.n_cols + 1):

            # Check columns
            for j in range(0, self.n_chars, 3):
                col = ""
                for k in range(self.n_cols):
                    col += self.boards[i + k][j : j + 2] + " "
            
                if not col.strip():
                    win = True

            # Check rows
            for j in range(i, i + self.n_cols):
                row = self.boards[j]
                if not row.strip():
                    win = True     

            if win:
                break 


        score = " ".join(self.boards[i - self.n_cols - 1 : i]).split(" ")
        score = sum([int(el) for el in score if el.isdigit()])
        return score if win else -1



sub = Submarine("example.txt")
# sub = Submarine("input.txt")
while (score := sub.check_win()) < 0:
    draw = sub.draw()

print(draw, score, score * draw)