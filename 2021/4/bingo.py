class Submarine:

    def __init__(self, input):

        with open(input) as f:
            self.draws = [int(el) for el in f.readline().strip().split(',')]
            self.boards = f.read().splitlines()#[1:]
            self.n_boards = len(self.boards)
            self.n_cols = len(self.boards[1].split())
            self.drawn = 100000
            self.skip = []

            for i in range(1, self.n_boards, self.n_cols + 1):
                board = []
                for j in range(self.n_cols):
                    board.append([int(el) for el in self.boards[i + j].split()])
                self.boards.append(board)
            self.boards = self.boards[self.n_boards:]
            self.n_boards = len(self.boards)


    def print_boards(self):
        
        print("----")
        for board in self.boards:
            for row in board:
                print(" ".join([str(el) for el in row]))
            print()
        print("----")


    def draw(self):

        number = self.draws.pop(0)
        for i in range(self.n_boards):
            for j in range(self.n_cols):
                self.boards[i][j] = [el if el != number else self.drawn for el in self.boards[i][j]]
        return number


    def check_win(self):

        score = -1
        for i, board in enumerate(self.boards):
            if i in self.skip:
                continue
            for row in board:
                if sum(row) % self.drawn == 0:
                    self.skip.append(i)
                    score = self.calc_score(board)
                    break

            for column in zip(*board):
                if sum(column) % self.drawn == 0:
                    self.skip.append(i)
                    score = self.calc_score(board)
                    break

        return score
                
    
    def calc_score(self, board):
        return sum([sum(row) % self.drawn for row in board])



sub = Submarine("example.txt")
sub = Submarine("input.txt")
wins = []
while len(sub.draws) > 0:
    draw = sub.draw()
    score = sub.check_win()
    if score > 0:
        wins.append([score * draw, score, draw])

print(wins)
print("Part 1", wins[0][0])
print("Part 2", wins[-1][0])
