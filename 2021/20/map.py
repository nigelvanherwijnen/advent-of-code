class Scan:

    def __init__(self, filename):
        with open(filename) as f:
            self.alg, self.map = f.read().split("\n\n")
        self.alg = self.alg.replace("#", "1").replace(".", "0")
        self.map = [list(line) for line in self.map.replace("#", "1").replace(".", "0").split("\n")]

        self.add_padding(5)
        self.print_map()


    def enhance(self):

        ind = 0 if self.map[0][0] == "0" else 511
        indices = [[ind for el in range(len(self.map[0]))] for el in range(len(self.map))]
        for row in range(1, len(self.map) - 1):
            for col in range(1, len(self.map[0]) - 1):

                window = self.get_window(row, col)
                ind = self.get_literal(window)
                indices[row][col] = ind             

        for row in range(0, len(self.map) - 0):
            for col in range(0, len(self.map[0]) - 0):
                self.map[row][col] = self.alg[indices[row][col]]

    
    def get_window(self, row, col):

        window = []
        for i in range(row - 1, row + 2):
            window += self.map[i][col - 1 : col + 2]
        return window


    def add_padding(self, padding=1, sign=None):

        if sign == None:
            sign = "0"

        for i in range(padding):
            self.map = [[sign for el in range(len(self.map[0]))]] + self.map
            self.map.append([sign for el in range(len(self.map[0]))])

            for j in range(len(self.map)):
                self.map[j] = [sign] + self.map[j]
            for j in range(len(self.map)):
                self.map[j] = self.map[j] + [sign]


    def get_literal(self, binary):
        binary = [int(el) for el in binary]
        val = 0
        for i in range(len(binary)):
            val += binary[-1 - i] * (2 ** i)
        return val


    def get_sum(self):
        count = 0
        for row in range(len(self.map)):
            for col in range(len(self.map[0])):
                count += int(self.map[row][col])
        return count
    
    def print_map(self):

        for row in self.map:
            print("".join(row).replace("1", "#").replace("0", "."))
        print()





scan = Scan("example.txt")
scan = Scan("input.txt")

for i in range(50):
    scan.enhance()
    scan.add_padding(2, scan.map[0][0])
    scan.print_map()

print(scan.get_sum())
