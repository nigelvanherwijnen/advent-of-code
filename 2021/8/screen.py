from pprint import pprint
class Screen:

    def __init__(self, filename):

        with open(filename) as f:
            self.entries = [el.split(" | ") for el in f.read().splitlines()]

        self.entries = [{
            "signal":   ["".join(sorted(signal)) for signal in el[0].split()],
            "output":   ["".join(sorted(signal)) for signal in el[1].split()],
            "solution": [-1 for i in range(len(el[1].split()))],
            "digits": 0
        } for el in self.entries]

        # self.entries = [self.entries[0]]
        # pprint(self.entries)


    def retrieve_unique(self, signals):

        unique = {
            2: 1,
            4: 4,
            3: 7,
            7: 8
        }

        return [unique[len(signal)] if len(signal) in unique.keys() else -1 for signal in signals]


    def decrypt(self):


        for entry in self.entries:

            # Get unique ones
            entry["solution"] = self.retrieve_unique(entry["signal"])

            for i in range(len(entry["solution"])):

                # Skip unique digits
                if entry["solution"][i] >= 0:
                    continue

                # Codes with length 5
                if len(entry["signal"][i]) == 5:
                    # print(entry["signal"][i])

                    count = 0
                    for signal in entry["signal"]:
                        if len(signal) == 6:
                            count += int(sum([1 for el in entry["signal"][i] if el in signal]) == len(entry["signal"][i]))

                    if count == 0:
                        entry["solution"][i] = 2
                    elif count == 1:
                        entry["solution"][i] = 3
                    elif count == 2:
                        entry["solution"][i] = 5

                # Code with length 6
                elif len(entry["signal"][i]) == 6:

                    count = 0
                    for signal in entry["signal"]:
                        if len(signal) < 6:
                            count += int(sum([1 for el in entry["signal"][i] if el in signal]) == len(signal))


                    if count == 1:
                        entry["solution"][i] = 6
                    elif count == 2:
                        entry["solution"][i] = 0
                    elif count == 5:
                        entry["solution"][i] = 9

            for i, pattern in enumerate(reversed(entry["output"])):
                # print(10 ** i, pattern)
                for j, signal in enumerate(entry["signal"]):
                    if signal == pattern:
                        entry["digits"] += (10 ** i) * entry["solution"][j]
                        break

            # pprint(entry)

        summed = sum([el["digits"] for el in self.entries]) 
        print(summed)






screen = Screen("example.txt")
screen = Screen("input.txt")

# screen.retrieve_unique(screen.entries[0]["output"])
screen.decrypt()