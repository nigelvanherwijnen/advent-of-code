class Submarine:

    def __init__(self, file):
    
        with open(file) as f:
            self.data = f.read().splitlines()
            self.data = [int(item) for item in self.data]
            # print(self.data)


    def scan(self):

        increments = [1 for i in range(1, len(self.data)) if self.data[i] > self.data[i - 1] ]
        return sum(increments)


    def scan_window(self, window=3):

        increments = [1 for i in range(window - 1, len(self.data)) \
            if sum(self.data[i - window:i]) > sum(self.data[i - window - 1:i - 1])]
        return sum(increments)


sub = Submarine("example.txt")
sub = Submarine("input.txt")
part_1 = sub.scan()
part_2 = sub.scan_window()
print(part_1)
print(part_2)