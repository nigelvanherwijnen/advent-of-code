class Decoder:

    def __init__(self, filename):

        with open(filename) as f:
            packet = f.read().splitlines()
        self.packet = packet

        with open("coding.txt") as f:
            coding = f.read().splitlines()
        self.coding = dict()
        for code in coding:
            key, bit = code.split(" = ")
            self.coding[key] = [int(b) for b in list(bit)]


    def decode(self, binary):

        version = self.get_literal(binary[0:3])
        type_id = self.get_literal(binary[3:6])

        if type_id == 4:

            last = False
            bit_start = 6
            bin = []
            while last == False:
                last = True if binary[bit_start] == 0 else False 
                bin += binary[bit_start + 1 : bit_start + 5]
                bit_start += 5
            return version, bit_start, self.get_literal(bin)

        length_type = binary[6]
        values = []
        if length_type == 0:

            sub_start, end = 22, 0
            length = self.get_literal(binary[7:sub_start])
            while sub_start + end < sub_start + length:
                v, new, value = self.decode(binary[sub_start + end : sub_start + length])
                end += new
                version += v
                values.append(value)
        
        else:
            sub_start, end = 18, 0
            n_subs = self.get_literal(binary[7:sub_start])
            for i in range(n_subs):
                v, new, value = self.decode(binary[sub_start + end : :])
                end += new
                version += v
                values.append(value)

        if type_id == 0:
            values = sum(values)
        elif type_id == 1:
            value = 1
            for el in values:
                value *= el 
            values = value
        elif type_id == 2:
            values = min(values)
        elif type_id == 3:
            values = max(values)
        elif type_id == 5:
            values = 1 if values[0] > values[1] else 0
        elif type_id == 6:
            values = 1 if values[0] < values[1] else 0
        elif type_id == 7:
            values = 1 if values[0] == values[1] else 0

        return version, sub_start + end, values


    def get_literal(self, binary):
        val = 0
        for i in range(len(binary)):
            val += binary[-1 - i] * (2 ** i)
        return val

        
    def get_binary(self, packet):
        binary = []
        for hex in packet:
            binary += self.coding[hex]
        return binary


decoder = Decoder("example.txt")
decoder = Decoder("input.txt")

for packet in decoder.packet:
    binary = decoder.get_binary(packet)
    version, end, value = decoder.decode(binary)
    print(version, value)